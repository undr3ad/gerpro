from django.urls import path
from . import views

urlpatterns = [
    path('', views.EtapaListView.as_view(), name='etapa_list'),
    path('new/', views.EtapaCreateView.as_view(), name='etapa_new'),
    path('<int:pk>/edit/', views.EtapaUpdateView.as_view(), name='etapa_edit'),
    path('<int:pk>/detail/', views.EtapaDetailView.as_view(), name='etapa_detail'),
    path('<int:pk>/delete/', views.EtapaDeleteView.as_view(), name='etapa_delete'),
    path('acoes/', views.AcaoListView.as_view(), name='acao_list'),
    path('acoes/new/', views.AcaoCreateView.as_view(), name='acao_new'),
    path('acoes/<int:pk>/edit/', views.AcaoUpdateView.as_view(), name='acao_edit'),
    path('acoes/<int:pk>/detail/', views.AcaoDetailView.as_view(), name='acao_detail'),
    path('acoes/<int:pk>/delete/', views.AcaoDeleteView.as_view(), name='acao_delete'),
]
