from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from . import models


class EtapaListView(LoginRequiredMixin, ListView):
    model = models.Etapa
    template_name = 'etapa_list.html'
    login_url = 'login'


class EtapaCreateView(LoginRequiredMixin, CreateView):
    model = models.Etapa
    template_name = 'etapa_new.html'
    fields = ['nome', 'descricao', 'ordem', 'is_active']
    login_url = 'login'


class EtapaDetailView(LoginRequiredMixin, DetailView):
    model = models.Etapa
    template_name = 'etapa_detail.html'
    login_url = 'login'


class EtapaUpdateView(LoginRequiredMixin, UpdateView):
    model = models.Etapa
    template_name = 'etapa_edit.html'
    fields = ['nome', 'descricao', 'ordem', 'distribuicao', 'vincular_analista', 'is_active']
    login_url = 'login'


class EtapaDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Etapa
    template_name = 'etapa_delete.html'
    success_url = reverse_lazy('etapa_list')
    login_url = 'login'


class AcaoListView(LoginRequiredMixin, ListView):
    model = models.Acao
    template_name = 'acao_list.html'
    login_url = 'login'


class AcaoCreateView(LoginRequiredMixin, CreateView):
    model = models.Acao
    template_name = 'acao_new.html'
    fields = ['nome', 'descricao', 'etapa_seguinte', 'finalizar', 'is_active']
    login_url = 'login'


class AcaoDetailView(LoginRequiredMixin, DetailView):
    model = models.Acao
    template_name = 'acao_detail.html'
    login_url = 'login'


class AcaoUpdateView(LoginRequiredMixin, UpdateView):
    model = models.Acao
    template_name = 'acao_edit.html'
    fields = ['nome', 'descricao', 'etapa_seguinte', 'finalizar', 'is_active']
    login_url = 'login'


class AcaoDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Acao
    template_name = 'acao_delete.html'
    success_url = reverse_lazy('acao_list')
    login_url = 'login'
