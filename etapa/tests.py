from django.test import TestCase
from accounts.models import ProjectUser
from django.urls import reverse
from etapa.models import Etapa


class EtapaTeste(TestCase):

    def setUp(self):
        self.user = ProjectUser.objects.create_user('asdf', 'teste@teste.com', 'asdf')
        Etapa.objects.create(nome='teste1', descricao='teste1', is_active=True)
        Etapa.objects.create(nome='teste2', descricao='teste2', is_active=False)

    def test_setUp_created_objects(self):
        self.client.login(username='asdf', password='asdf')
        response1 = Etapa.objects.get(id='1')
        self.assertEqual(response1.nome, 'teste1')
        self.assertEqual(response1.descricao, 'teste1')
        self.assertEqual(response1.is_active, True)
        response2 = Etapa.objects.get(id='2')
        self.assertEqual(response2.nome, 'teste2')
        self.assertEqual(response2.descricao, 'teste2')
        self.assertEqual(response2.is_active, False)

    def test_login(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_etapa_list_view_status_code(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_etapa_list_view_url_by_name(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('etapa_list'))
        self.assertEqual(response.status_code, 200)

    def test_etapa_list_view_uses_correct_template(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('etapa_list'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'etapa_list.html')

    def test_etapa_create_view_url_by_name(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('etapa_new'))
        self.assertEqual(response.status_code, 200)

    def test_etapa_create_view_uses_correct_template(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('etapa_new'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'etapa_new.html')

    def test_etapa_create_view_create_objects(self):
        self.client.login(username='asdf', password='asdf')
        Etapa.objects.create(nome='teste3', descricao='teste3', is_active=True)
        response = Etapa.objects.get(id='3')
        self.assertEqual(response.nome, 'teste3')
        self.assertEqual(response.descricao, 'teste3')
        self.assertEqual(response.is_active, True)

    def test_etapa_get_absolute_url(self):
        response = Etapa.objects.get(id='1')
        self.assertEqual(response.get_absolute_url(), '/etapas/1/detail/')
