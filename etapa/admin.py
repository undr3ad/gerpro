from django.contrib import admin
from . import models


class AcaoInline(admin.TabularInline):
    model = models.Acao
    fk_name = 'etapa'


class EtapaAdmin(admin.ModelAdmin):
    inlines = [
        AcaoInline,
    ]


admin.site.register(models.Etapa, EtapaAdmin)
admin.site.register(models.Acao)
