from django.db import models
from django.urls import reverse


class ActiveObjectsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)


class Etapa(models.Model):
    nome = models.CharField(
        max_length=200,
        verbose_name='Etapa',
        help_text='Nome da etapa.',
    )
    descricao = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name='Descrição da etapa',
        help_text='Descreve a função da etapa.',
    )
    ordem = models.PositiveSmallIntegerField(
        null=True,
        unique=True,
        verbose_name='Ordem',
        help_text='Define a posição/ordem da etapa.',
    )
    distribuicao = models.ManyToManyField(
        'distribuicao.TipoDistribuicao',
        db_index=True,
        blank=True,
        db_table='etapa_distribuicao',
        verbose_name='Distribuição',
        help_text='Indica quais tipos de distribuição podem ser usados para essa etapa.'
    )
    vincular_analista = models.BooleanField(
        default=False,
        verbose_name='Vincular analista',
        help_text='Indica se o sistema deve se lembrar do usuário escolhido na primeira distribuição do processo.'
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='Está ativa?',
        help_text='Indica se a etapa está ou não ativa.',
    )

    objects = models.Manager()
    active_objects = ActiveObjectsManager()

    class Meta:
        ordering = ['ordem', 'nome']

    def get_absolute_url(self):
        return reverse('etapa_detail', args=[str(self.id)])

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return self.nome


class Acao(models.Model):
    etapa = models.ForeignKey(
        Etapa,
        on_delete=models.CASCADE,
        verbose_name='Etapa',
        help_text='Etapa à qual a ação está vinculada.'
    )
    nome = models.CharField(
        max_length=200,
        verbose_name='Nome',
        help_text='Nome da ação.'
    )
    descricao = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name='Descrição',
        help_text='Descrição da ação.',
    )
    etapa_seguinte = models.ForeignKey(
        Etapa,
        related_name='etapa_seguinte',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name='Etapa seguinte',
        help_text='Etapa para a qual o processo deve ser movido.',
    )
    finalizar = models.BooleanField(
        default=False,
        verbose_name='Finalizar?',
        help_text='Indica se o protocolo deve ser finalizado ao executar a ação.'
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='Está ativa?',
        help_text='Indica se a ação está ou não ativa.',
    )

    objects = models.Manager()
    active_objects = ActiveObjectsManager()

    class Meta:
        ordering = ['nome', 'etapa']
        verbose_name = 'Ação'
        verbose_name_plural = 'Ações'

    def get_absolute_url(self):
        return reverse('etapa_detail', args=[str(self.id)])

    def __str__(self):
        return str(self.nome + " (" + self.etapa.nome + ")")

    def __unicode__(self):
        return str(self.nome + " (" + self.etapa.nome + ")")
