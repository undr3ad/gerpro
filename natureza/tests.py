from django.test import TestCase
from accounts.models import ProjectUser
from django.urls import reverse
from natureza.models import Natureza


class NaturezaTeste(TestCase):

    def setUp(self):
        self.user = ProjectUser.objects.create_user('asdf', 'teste@teste.com', 'asdf')
        Natureza.objects.create(nome='teste1', prazo='1', is_active=True)
        Natureza.objects.create(nome='teste2', prazo='2', is_active=False)

    def test_setUp_created_objects(self):
        self.client.login(username='asdf', password='asdf')
        response1 = Natureza.objects.get(id='1')
        self.assertEqual(response1.nome, 'teste1')
        self.assertEqual(response1.prazo, 1)
        self.assertEqual(response1.is_active, True)
        response2 = Natureza.objects.get(id='2')
        self.assertEqual(response2.nome, 'teste2')
        self.assertEqual(response2.prazo, 2)
        self.assertEqual(response2.is_active, False)

    def test_login(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_natureza_list_view_status_code(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_natureza_list_view_url_by_name(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('natureza_list'))
        self.assertEqual(response.status_code, 200)

    def test_natureza_list_view_uses_correct_template(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('natureza_list'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'natureza_list.html')

    def test_natureza_create_view_url_by_name(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('natureza_new'))
        self.assertEqual(response.status_code, 200)

    def test_natureza_create_view_uses_correct_template(self):
        self.client.login(username='asdf', password='asdf')
        response = self.client.get(reverse('natureza_new'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'natureza_new.html')

    def test_natureza_create_view_create_objects(self):
        self.client.login(username='asdf', password='asdf')
        Natureza.objects.create(nome='teste3', prazo='3', is_active=True)
        response = Natureza.objects.get(id='3')
        self.assertEqual(response.nome, 'teste3')
        self.assertEqual(response.prazo, 3)
        self.assertEqual(response.is_active, True)

    def test_natureza_get_absolute_url(self):
        response = Natureza.objects.get(id='1')
        self.assertEqual(response.get_absolute_url(), '/naturezas/1/detail/')
