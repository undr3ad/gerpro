# Generated by Django 2.0.3 on 2018-04-05 18:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('natureza', '0002_auto_20180405_1447'),
    ]

    operations = [
        migrations.AlterField(
            model_name='natureza',
            name='pk_import',
            field=models.IntegerField(blank=True, help_text='Indica que o registro foi importado de outro sistema e essa é sua chave primária naquele sistema.', null=True, verbose_name='ID Legado'),
        ),
        migrations.AlterField(
            model_name='natureza',
            name='prazo',
            field=models.IntegerField(blank=True, help_text='Prazo legal da prenotação. Período de validade oficial dos processos ligados à esta natureza.', null=True, verbose_name='Prazo legal'),
        ),
    ]
