from django.db import models
from django.urls import reverse


class Natureza(models.Model):
    nome = models.CharField(
        max_length=200,
        verbose_name='Nome da Natureza',
    )
    prazo = models.IntegerField(
        null=True,
        blank=True,
        default=30,
        verbose_name='Prazo legal',
        help_text='Prazo legal da prenotação. Período de validade oficial dos processos ligados à esta natureza.',
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='Está ativo?',
        help_text='Indica se a natureza está ou não ativa.',
    )
    pk_import = models.IntegerField(
        unique=True,
        null=True,
        blank=True,
        verbose_name='ID Legado',
        help_text='Indica que o registro foi importado de outro sistema e essa é sua chave primária naquele sistema.',
    )

    class Meta:
        ordering = ['nome']

    def get_absolute_url(self):
        return reverse('natureza_detail', args=[str(self.id)])

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return self.nome
