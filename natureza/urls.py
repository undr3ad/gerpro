from django.urls import path
from . import views

urlpatterns = [
    path('', views.NaturezaListView.as_view(), name='natureza_list'),
    path('new/', views.NaturezaCreateView.as_view(), name='natureza_new'),
    path('<int:pk>/edit/', views.NaturezaUpdateView.as_view(), name='natureza_edit'),
    path('<int:pk>/detail/', views.NaturezaDetailView.as_view(), name='natureza_detail'),
    path('<int:pk>/delete/', views.NaturezaDeleteView.as_view(), name='natureza_delete'),
]
