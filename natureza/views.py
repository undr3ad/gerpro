from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from . import models


class NaturezaListView(LoginRequiredMixin, ListView):
    model = models.Natureza
    template_name = 'natureza_list.html'
    login_url = 'login'


class NaturezaCreateView(LoginRequiredMixin, CreateView):
    model = models.Natureza
    template_name = 'natureza_new.html'
    fields = ['nome', 'prazo', 'is_active']
    login_url = 'login'


class NaturezaDetailView(LoginRequiredMixin, DetailView):
    model = models.Natureza
    template_name = 'natureza_detail.html'
    login_url = 'login'


class NaturezaUpdateView(LoginRequiredMixin, UpdateView):
    model = models.Natureza
    template_name = 'natureza_edit.html'
    fields = ['nome', 'prazo', 'is_active']
    login_url = 'login'


class NaturezaDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Natureza
    template_name = 'natureza_delete.html'
    success_url = reverse_lazy('natureza_list')
    login_url = 'login'
