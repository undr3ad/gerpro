from django.db import models


class TipoDistribuicao(models.Model):
    nome = models.CharField(
        max_length=70,
        verbose_name='Nome',
        help_text='Nome do tipo de distribuição.',
    )
    descricao = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name='Descrição',
        help_text='Descrição do tipo de distribuição.',
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='Está ativo?',
        help_text='Indica se o tipo de distribuição está ativo.',
    )

    class Meta:
        ordering = ['nome']
        verbose_name = 'Tipo de Distribuição'
        verbose_name_plural = 'Tipos de Distribuição'

    def __str__(self):
        return str(self.nome)
