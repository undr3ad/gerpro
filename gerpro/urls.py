from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('pages.urls')),
    path('dashboard/', include('dashboard.urls')),
    path('naturezas/', include('natureza.urls')),
    path('etapas/', include('etapa.urls')),
    path('processos/', include('processo.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
]
