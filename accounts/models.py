from django.db import models
from django.db.models import Count
from django.contrib.auth.models import AbstractUser, Group, UserManager, GroupManager


class ProjectUserManager(UserManager):
    pass


class ProjectGroupManager(GroupManager):
    pass


class ProjectUser(AbstractUser):
    receber_distribs = models.ManyToManyField(
        'distribuicao.TipoDistribuicao',
        db_index=True,
        blank=True,
        db_table='Distribuicoes_dos_Usuarios',
        verbose_name='Receber Distribuições',
        help_text='Indica quais tipos de distribuição o usuário pode receber.'
    )
    receber_procs = models.BooleanField(
        default=False,
        verbose_name='Receber Processos',
        help_text='Indica que o usuário pode receber processos da aplicação GERPRO.',
    )
    naturezas = models.ManyToManyField(
        'natureza.Natureza',
        db_index=True,
        blank=True,
        db_table='Naturezas_dos_Usuarios',
        verbose_name='Naturezas do Usuário',
        help_text='Naturezas às quais o usuário tem acesso.',
    )

    objects = ProjectUserManager()

    def get_etapas(instance):
        return ProjectUser.objects.filter(
            is_active=True,
            id=instance.id,
            groups__projectgroup__etapas__is_active=True,
        ).values(
            'groups__projectgroup__etapas__id',
            'groups__projectgroup__etapas__nome',
            'groups__projectgroup__etapas__descricao',
            'groups__projectgroup__etapas__is_active',
        ).distinct().order_by('groups__projectgroup__etapas__ordem',)

    def get_analistas_proc_count():
        """Retorna um queryset com dicionario de usuários e número de protocolos distribuídos a eles."""
        return ProjectUser.objects.filter(
            processo__is_active=True,
            is_active=True,
            processo__etapa__is_active=True,
            processo__etapa__distribuicao__isnull=False
        ).annotate(proc_qtd=Count('processo__l1_protocolo', distinct=True))

    def get_processos(instance):
        """ query para retornar todos os processos do usuário provenientes de etapas não personificadas"""
        q1 = ProjectUser.objects.filter(
            is_active=True,
            id=instance.id,
            groups__projectgroup__etapas__is_active=True,
            groups__projectgroup__etapas__distribuicao__isnull=True,
            groups__projectgroup__etapas__processo__is_active=True,
        ).values(
            'groups__projectgroup__etapas__processo__id',
            'groups__projectgroup__etapas__processo__l1_protocolo',
            'groups__projectgroup__etapas__processo__natureza',
            'groups__projectgroup__etapas__processo__natureza__nome',
            'groups__projectgroup__etapas__processo__dta_inclusao',
            'groups__projectgroup__etapas__processo__dta_entrega',
            'groups__projectgroup__etapas__processo__dta_prazo',
            'groups__projectgroup__etapas__processo__etapa',
            'groups__projectgroup__etapas__processo__etapa__nome',
            'groups__projectgroup__etapas__processo__parte',
            'groups__projectgroup__etapas__processo__atendente',
            'groups__projectgroup__etapas__processo__agente',
            'groups__projectgroup__etapas__processo__agente__username',
            'groups__projectgroup__etapas__processo__vinculo',
            'groups__projectgroup__etapas__processo__is_active',
        ).distinct()
        """query para retornar todos os processos do usuário provenientes de etapas
        personificadas e que tenham sido distribuidos para o usuario"""
        """q2 = ProjectUser.objects.filter(
            is_active=True,
            id=instance.id,
            groups__projectgroup__etapas__is_active=True,
            groups__projectgroup__etapas__processo__agente=instance.id,
            groups__projectgroup__etapas__distribuicao__isnull=False,
            groups__projectgroup__etapas__processo__is_active=True,
        ).values(
            'groups__projectgroup__etapas__processo__id',
            'groups__projectgroup__etapas__processo__l1_protocolo',
            'groups__projectgroup__etapas__processo__natureza',
            'groups__projectgroup__etapas__processo__natureza__nome',
            'groups__projectgroup__etapas__processo__dta_inclusao',
            'groups__projectgroup__etapas__processo__dta_entrega',
            'groups__projectgroup__etapas__processo__dta_prazo',
            'groups__projectgroup__etapas__processo__etapa',
            'groups__projectgroup__etapas__processo__etapa__nome',
            'groups__projectgroup__etapas__processo__parte',
            'groups__projectgroup__etapas__processo__atendente',
            'groups__projectgroup__etapas__processo__agente',
            'groups__projectgroup__etapas__processo__agente__username',
            'groups__projectgroup__etapas__processo__vinculo',
            'groups__projectgroup__etapas__processo__is_active',
        ).distinct()"""
        q2 = ProjectUser.objects.filter(
            processo__is_active=True,
            is_active=True,
            processo__agente=instance.id,
            processo__etapa__is_active=True,
            processo__etapa__distribuicao__isnull=False
        ).values(
            'processo__id',
            'processo__l1_protocolo',
            'processo__natureza',
            'processo__natureza__nome',
            'processo__dta_inclusao',
            'processo__dta_entrega',
            'processo__dta_prazo',
            'processo__etapa',
            'processo__etapa__nome',
            'processo__parte',
            'processo__atendente',
            'processo__agente',
            'processo__agente__username',
            'processo__vinculo',
            'processo__is_active',
        )
        return q1.union(q2).order_by('groups__projectgroup__etapas__processo__l1_protocolo',)


class ProjectGroup(Group):
    etapas = models.ManyToManyField(
        'etapa.Etapa',
        db_index=True,
        blank=True,
        db_table='Etapas_dos_Grupos',
        verbose_name='Etapas dos Grupos',
        help_text='Etapas às quais os grupos estão vinclulados.',
    )

    objects = ProjectGroupManager()

    class Meta:
        verbose_name = 'Grupo'
        verbose_name_plural = 'Grupos'
