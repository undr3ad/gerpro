from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, GroupAdmin, Group

from .forms import ProjectUserCreationForm, ProjectUserChangeForm
from .models import ProjectUser, ProjectGroup


class ProjectUserAdmin(UserAdmin):
    model = ProjectUser
    form = ProjectUserChangeForm
    add_form = ProjectUserCreationForm

    fieldsets = UserAdmin.fieldsets + (
        ('GERPRO', {
            'fields': (
                'receber_procs',
                'receber_distribs',
                'naturezas',
            )
        }),
    )


class ProjectGroupAdmin(GroupAdmin):
    model = ProjectGroup


admin.site.register(ProjectUser, ProjectUserAdmin)
admin.site.unregister(Group)
admin.site.register(ProjectGroup, ProjectGroupAdmin)
