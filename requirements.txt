Django==2.1
django-auth-ldap==1.7.0
django-crispy-forms==1.7.2
pyasn1==0.4.4
pyasn1-modules==0.2.2
python-ldap==3.1.0
pytz==2018.5
