from django.urls import path
from . import views

urlpatterns = [
    path('', views.DashboardView.as_view(), name='dashboard_view'),
    path('valida_acao', views.ValidaAcao.as_view(), name='valida_acao'),
]
