from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.views import View
from etapa.models import Etapa, Acao
from accounts.models import ProjectUser
from processo.models import Processo
from django.http import JsonResponse


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard_view.html'

    def get_user_etapas(self):
        userid = self.request.user.id
        return ProjectUser.objects.get(id=userid).get_etapas()

    def get_user_etapa_acoes(self):
        etapasid = self.get_user_etapas().values_list('groups__projectgroup__etapas__id', flat=True)
        return Etapa.objects.filter(
            is_active=True,
            id__in=etapasid,
            acao__is_active=True,
        ).values(
            'acao__id',
            'acao__etapa',
            'acao__nome',
            'acao__descricao',
            'acao__etapa_seguinte',
            'acao__finalizar',
            'acao__is_active',
        ).distinct().order_by('acao__nome',)

    def get_user_processos(self):
        userid = self.request.user.id
        return ProjectUser.objects.get(id=userid).get_processos()


class ValidaAcao(LoginRequiredMixin, View):

    def get_etapa_seg_dists(self, acao):
        etapa_seguinte_distribuicoes = []

        if len(acao) > 0:
            count = 0
            while (count < len(acao)):
                if acao[count]['etapa_seguinte__distribuicao']:
                    etapa_seguinte_distribuicoes.append(acao[count]['etapa_seguinte__distribuicao'])
                count = count + 1

        return etapa_seguinte_distribuicoes

    def get_ana_listas(self, processo_id, acao):
        processo = Processo.objects.get(id=processo_id)
        etapa_seguinte_distribuicoes = self.get_etapa_seg_dists(acao)
        ana_lista_qtd = ProjectUser.get_analistas_proc_count()

        ana_lista = list(ProjectUser.objects.filter(
            receber_distribs__in=etapa_seguinte_distribuicoes,
            receber_procs=True,
            naturezas=processo.natureza
        ).exclude(id=self.request.user.id).values('username', 'id').distinct())

        count_al = 0
        while (count_al < len(ana_lista)):
            count_al_qtd = 0
            ana_lista[count_al]['proc_qtd'] = 0
            while (count_al_qtd < len(ana_lista_qtd)):
                if ana_lista[count_al]['id'] == ana_lista_qtd[count_al_qtd].id:
                    ana_lista[count_al]['proc_qtd'] = ana_lista_qtd[count_al_qtd].proc_qtd
                count_al_qtd = count_al_qtd + 1
            count_al = count_al + 1

        return list(ana_lista)

    def get(self, request):
        acao = Acao.objects.filter(
            id=self.request.GET['acao']
        ).values('id', 'etapa', 'etapa_seguinte', 'etapa_seguinte__nome', 'etapa_seguinte__distribuicao', 'finalizar')
        analista = self.request.GET['analista']
        #usuario = ProjectUser.objects.get(id=self.request.user.id).username

        """A ação executada sempre retornará 2 valores 3 um terceiro valor opcional.
        Os dois valores são o id da ação, de onde identifica-se a etapa e etapa_seguinte, e o protocolo.
        De posse desses dois parâmetos a view retorna valores para o template. Esses valores podem ser
        a confirmação da execução da ação ou a lista de analistas elegíveis no caso em que se perceba a
        necessidade de distribuição.

        Uma vez enviada a lista de analistas, parte do template responder com um analista selecionado.
        Nesse caso, o terceiro valor (analista) é informado, e esta view executa a distribuição e informa
        o template."""

        etapa_seguinte_distribuicoes = self.get_etapa_seg_dists(acao)

        """Já com a lista de distribuições da etapa seguinte, não sendo informado o analista selecionado para a
        distribuição, cria-se uma lista de analistas elegíveis para a distribuição."""
        if len(etapa_seguinte_distribuicoes) > 0 and len(analista) < 1:
            analistas_selecao = self.get_ana_listas(self.request.GET['processo'], acao)
        else:
            analistas_selecao = None

        """No caso do analista ser informado, o sistema executa a distribuição."""
        if len(analista) == 1 and len(etapa_seguinte_distribuicoes) > 0:
            proc_distrib = list(ProjectUser.objects.filter(id=analista,).values('username', 'id').distinct())
        else:
            proc_distrib = None

        """Retorna as variáveis ao template, que as tratará de acordo com os requisitos do negócio."""
        return JsonResponse(
            {
                "analistas": analistas_selecao,
                "proc_etapa": acao[0]['etapa_seguinte__nome'],
                "proc_active": not acao[0]['finalizar'],
                "proc_distrib": proc_distrib,
            }
        )
