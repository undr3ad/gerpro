from django import forms
from .models import Processo


class ProcessoChangeForm(forms.ModelForm):
    class Meta:
        model = Processo
        fields = (
            'l1_protocolo',
            'dta_inclusao',
            'dta_prazo',
            'natureza',
            'etapa',
            'parte',
            'atendente',
            'agente',
        )
    l1_protocolo = forms.IntegerField(
        disabled=True,
        label='Processo',
        help_text='Número do processo/protocolo.',
    )
    dta_inclusao = forms.DateField(
        disabled=True,
        label='Data de entrada',
        help_text='Data em que o processo deu entrada (inclusão).'
    )
    dta_prazo = forms.DateField(
        disabled=True,
        label='Vencimento',
        help_text='Data em que vence o prazo legal do processo.'
    )
    parte = forms.CharField(
        disabled=True,
        label='Parte',
        help_text='Nome/identificação da parte/cliente.',
    )
    atendente = forms.CharField(
        disabled=True,
        label='Atendente',
        help_text='Nome/identificação do funcionário que deu entrada(incluiu) no processo.',
    )
    agente = forms.CharField(
        disabled=True,
        label='Agente',
        help_text='Nome/identificação do funcionário responsável pelo processo na presente etapa.',
    )
