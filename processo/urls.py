from django.urls import path
from . import views

urlpatterns = [
    path('', views.ProcessoListView.as_view(), name='processo_list'),
    path('new/', views.ProcessoCreateView.as_view(), name='processo_new'),
    path('<int:pk>/edit/', views.ProcessoUpdateView.as_view(), name='processo_edit'),
    path('<int:pk>/detail/', views.ProcessoDetailView.as_view(), name='processo_detail'),
    path('<int:pk>/delete/', views.ProcessoDeleteView.as_view(), name='processo_delete'),
]
