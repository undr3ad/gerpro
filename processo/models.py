from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse


class Processo(models.Model):
    l1_protocolo = models.IntegerField(
        unique=True,
        db_index=True,
        verbose_name='Processo',
        help_text='Número do processo/protocolo.'
    )
    natureza = models.ForeignKey(
        'natureza.Natureza',
        on_delete=models.PROTECT,
        verbose_name='Natureza',
        help_text='Natureza do processo.'
    )
    dta_inclusao = models.DateField(
        null=True,
        blank=True,
        verbose_name='Data de entrada',
        help_text='Data em que o processo deu entrada (inclusão).'
    )
    dta_entrega = models.DateField(
        null=True,
        blank=True,
        verbose_name='Previsão de entrega',
        help_text='Data da previsão de entrega do processo à parte.'
    )
    dta_prazo = models.DateField(
        null=True,
        blank=True,
        verbose_name='Vencimento',
        help_text='Data em que vence o prazo legal do processo.'
    )
    etapa = models.ForeignKey(
        'etapa.Etapa',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name='Etapa',
        help_text='Etapa do processo.'
    )
    parte = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name='Parte',
        help_text='Nome/identificação da parte/cliente.'
    )
    atendente = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name='Atendente',
        help_text='Nome/identificação do funcionário que deu entrada(incluiu) no processo.'
    )
    agente = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Agente',
        help_text='Nome/identificação do funcionário responsável pelo processo na presente etapa.'
    )
    vinculo = models.UUIDField(
        null=True,
        blank=True,
        verbose_name='Vínculo',
        help_text='Indica o vículo existente entre este e outros procesos.',
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='Está ativo?',
        help_text='Indica se o processo está ativo.'
    )

    class Meta:
        ordering = ['l1_protocolo']

    def get_absolute_url(self):
        return reverse('processo_detail', args=[str(self.id)])

    def __str__(self):
        return str(self.l1_protocolo)


class ProcessoAnalistaEtapa(models.Model):
    l1_protocolo = models.ForeignKey(
        Processo,
        on_delete=models.CASCADE,
        verbose_name='Processo',
        help_text='Número do processo/protocolo.'
    )
    etapa = models.ForeignKey(
        'etapa.Etapa',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Etapa',
        help_text='Etapa do processo.'
    )
    analista = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Analista',
        help_text='Nome/identificação do funcionário responsável pela análise do processo.'
    )

    class Meta:
        ordering = ['l1_protocolo']
        unique_together = ('l1_protocolo', 'etapa')
        verbose_name = 'Analista do Processo na Etapa'
        verbose_name_plural = 'Analistas dos Processos por Etapa'

    def __str__(self):
        return "[ " + str(self.l1_protocolo) + " - " + str(self.etapa) + " ] " + str(self.analista)
