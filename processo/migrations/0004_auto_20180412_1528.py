# Generated by Django 2.0.3 on 2018-04-12 18:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('processo', '0003_auto_20180412_1525'),
    ]

    operations = [
        migrations.AlterField(
            model_name='processo',
            name='l1_protocolo',
            field=models.IntegerField(db_index=True, editable=False, help_text='Número do processo/protocolo.', unique=True, verbose_name='Processo'),
        ),
    ]
