from django.contrib import admin
from . import models

admin.site.register(models.Processo)
admin.site.register(models.ProcessoAnalistaEtapa)
