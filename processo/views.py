from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from . import models
from .forms import ProcessoChangeForm


class ProcessoListView(LoginRequiredMixin, ListView):
    model = models.Processo
    template_name = 'processo_list.html'
    login_url = 'login'


class ProcessoCreateView(LoginRequiredMixin, CreateView):
    model = models.Processo
    template_name = 'processo_new.html'
    fields = ['l1_protocolo', 'natureza']
    login_url = 'login'


class ProcessoDetailView(LoginRequiredMixin, DetailView):
    model = models.Processo
    template_name = 'processo_detail.html'
    login_url = 'login'


class ProcessoUpdateView(LoginRequiredMixin, UpdateView):
    model = models.Processo
    template_name = 'processo_edit.html'
    form_class = ProcessoChangeForm
    login_url = 'login'


class ProcessoDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Processo
    template_name = 'processo_delete.html'
    success_url = reverse_lazy('processo_list')
    login_url = 'login'
